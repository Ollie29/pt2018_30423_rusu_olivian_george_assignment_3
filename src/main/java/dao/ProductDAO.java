package dao;

import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

public class ProductDAO {

    protected static final Logger LOGGER = Logger.getLogger(ProductDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO product (name,price,quantity)"
            + " VALUES (?,?,?)";
    private final static String findStatementString = "SELECT * FROM product where id = ?";
    private final static String allProductsStatementString = "SELECT * FROM product ORDER BY name";
    private final static String deleteStatementString = "DELETE FROM product where id = ?";
    private final static String updateStatementString = "UPDATE product SET name = ?,"
            + "quantity = ?, price = ? WHERE id = ?";
    private final static String inStockProductsStatementString = "SELECT * FROM product WHERE quantity > 0 ORDER BY name";
    

    public static Product findById(int productID) {
        Product toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, productID);
            rs = findStatement.executeQuery();
            rs.next();

            String name = rs.getString("name");
            float price = rs.getFloat("price");
            int quantity = rs.getInt("quantity");
            toReturn = new Product(productID, name, price, quantity);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static int insert(Product product) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, product.getName());
            insertStatement.setFloat(2, product.getPrice());
            insertStatement.setInt(3, product.getQuantity());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
                product.setId(insertedId);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    public static ArrayList<Product> getAll() {
        return get(allProductsStatementString, "getAll");
    }
    
    public static ArrayList<Product> getInStock() {
        return get(inStockProductsStatementString, "getInStock");
    }

    public static void deleteProduct(Product p) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setInt(1, p.getId());
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:deleteProduct " + e.getMessage());
        } finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
    }

    public static boolean update(Product product) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement updateStatement = null;
        int rowsAffected = 0;
        try {
            updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
            updateStatement.setString(1, product.getName());
            updateStatement.setInt(2, product.getQuantity());
            updateStatement.setFloat(3, product.getPrice());
            updateStatement.setInt(4, product.getId());
            rowsAffected = updateStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:update " + e.getMessage());
        } finally {
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }
        return (rowsAffected > 0);
    }

    private static ArrayList<Product> get(String statementString, String methodName) {
        ArrayList<Product> toReturn = new ArrayList<>();

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement allProductsStatement = null;
        ResultSet rs = null;
        try {
            allProductsStatement = dbConnection.prepareStatement(statementString);
            rs = allProductsStatement.executeQuery();
            while (rs.next()) {
                int productID = rs.getInt("id");
                String name = rs.getString("name");
                float price = rs.getFloat("price");
                int quantity = rs.getInt("quantity");
                toReturn.add(new Product(productID, name, price, quantity));
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:"+methodName+" " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(allProductsStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }
}
