package dao;

import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

public class CustomerDAO {

    protected static final Logger LOGGER = Logger.getLogger(CustomerDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO customer (name,address)"
            + " VALUES (?,?)";
    private final static String findStatementString = "SELECT * FROM customer where id = ?";
    private final static String allCustomersStatementString = "SELECT * FROM customer ORDER BY name";
    private final static String deleteStatementString = "DELETE FROM customer where id = ?";
    private final static String updateStatementString = "UPDATE customer SET name = ?,"
            + "address = ? WHERE id = ?";

    public static Customer findById(int customerID) {
        Customer toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, customerID);
            rs = findStatement.executeQuery();
            rs.next();

            String name = rs.getString("name");
            String address = rs.getString("address");
            toReturn = new Customer(customerID, name, address);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "CustomerDAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static ArrayList<Customer> getAll() {
        ArrayList<Customer> toReturn = new ArrayList<>();

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement allCustomersStatement = null;
        ResultSet rs = null;
        try {
            allCustomersStatement = dbConnection.prepareStatement(allCustomersStatementString);
            rs = allCustomersStatement.executeQuery();
            while (rs.next()) {
                int customerID = rs.getInt("id");
                String name = rs.getString("name");
                String address = rs.getString("address");
                toReturn.add(new Customer(customerID, name, address));
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "CustomerDAO:getAll " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(allCustomersStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static int insert(Customer customer) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, customer.getName());
            insertStatement.setString(2, customer.getAddress());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
                customer.setId(insertedId);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "CustomerDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    public static void deleteCustomer(Customer cust) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setInt(1, cust.getId());
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "CustomerDAO:deleteCustomer " + e.getMessage());
        } finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
    }
    public static boolean update(Customer customer) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement updateStatement = null;
        int rowsAffected = 0;
        try {
            updateStatement = dbConnection.prepareStatement(updateStatementString, Statement.RETURN_GENERATED_KEYS);
            updateStatement.setInt(3, customer.getId());
            updateStatement.setString(1, customer.getName());
            updateStatement.setString(2, customer.getAddress());
            rowsAffected = updateStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "CustomerDAO:update " + e.getMessage());
        } finally {
            ConnectionFactory.close(updateStatement);
            ConnectionFactory.close(dbConnection);
        }
        return (rowsAffected > 0);
    }
}
