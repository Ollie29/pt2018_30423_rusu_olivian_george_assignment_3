/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Date;

public class OrderFull {
    private int orderId;
    private String customer;
    private int customerId;
    private String product;
    private int productId;
    private int quantity;
    private float totalPrice;
    private Date dateOfOrder;

    public OrderFull(int orderId, String customer, int customerId, String product, int productId, int quantity, float totalPrice, Date dateOfOrder) {
        this.orderId = orderId;
        this.customer = customer;
        this.customerId = customerId;
        this.product = product;
        this.productId = productId;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.dateOfOrder = dateOfOrder;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getDateOfOrder() {
        return dateOfOrder;
    }

    public void setDateOfOrder(Date dateOfOrder) {
        this.dateOfOrder = dateOfOrder;
    }

}
