package dao;

import connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Order;

public class OrderDAO {

    protected static final Logger LOGGER = Logger.getLogger(OrderDAO.class.getName());
    private static final String insertStatementString
            = "INSERT INTO `order` (productId,clientId,quantity,date)"
            + " VALUES (?,?,?,?)";  // `...` are necessary here because order is a reserved keyword in mysql
    private final static String findStatementString = "SELECT * FROM `order` where id = ?";
    private final static String deleteProductsStatementString = "DELETE FROM `order` where productId = ?";
    private final static String deleteCustomersStatementString = "DELETE FROM `order` where clientId = ?";
    private final static String deleteOrderStatementString = "DELETE FROM `order` where id = ?";
    private static final String allOrdersStatementString = "SELECT `order`.id AS orderId, "
            + "customer.name AS cust, product.name AS prod, customer.id AS customerId, "
            + "`order`.quantity AS quantity, product.price AS totalPrice, `order`.date "
            + "AS dateOfOrder, product.id AS productId FROM customer, product, `order` "
            + "WHERE ((customer.id = `order`.clientId) AND (product.id = `order`.productId))"
            + " ORDER BY `order`.date";
    
    public static Order findById(int orderID) {
        Order toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, orderID);
            rs = findStatement.executeQuery();
            rs.next();

            int productId = rs.getInt("productId");
            int clientId = rs.getInt("clientId");
            int quantity = rs.getInt("quantity");
            Date date = rs.getDate("date");
            toReturn = new Order(orderID, productId, clientId, quantity, date);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static int insert(Order order) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setInt(1, order.getProductId());
            insertStatement.setInt(2, order.getClientId());
            insertStatement.setInt(3, order.getQuantity());
            insertStatement.setDate(4, order.getDate());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
                order.setId(insertedId);
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    public static void deleteCustomerOrders(int customerId) {
        delete(customerId, deleteCustomersStatementString, "deleteCustomerOrders");
    }

    public static void deleteProductOrders(int productId) {
        delete(productId, deleteProductsStatementString, "deleteProductOrders");
    }

    public static void deleteOrder(Order order) {
        delete(order.getId(), deleteOrderStatementString, "deleteOrder");
    }

    private static void delete(int id, String deleteStatementString, String command) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setInt(1, id);
            deleteStatement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:" + command + " " + e.getMessage());
        } finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
    }

    public static ArrayList<OrderFull> getAll() {
        ArrayList<OrderFull> toReturn = new ArrayList<>();

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement allOrdersStatement = null;
        ResultSet rs = null;
        try {
            allOrdersStatement = dbConnection.prepareStatement(allOrdersStatementString);
            rs = allOrdersStatement.executeQuery();
            while (rs.next()) {
                int orderId = rs.getInt("orderId");
                int customerId = rs.getInt("customerId");
                int productId = rs.getInt("productId");
                String customer = rs.getString("cust");
                String product = rs.getString("prod");
                float totalPrice = rs.getFloat("totalPrice");
                int quantity = rs.getInt("quantity");
                totalPrice *= quantity; // compute the price for this quantity
                Date dateOfOrder = rs.getDate("dateOfOrder");
                toReturn.add(new OrderFull(orderId, customer, customerId, product, productId, quantity, totalPrice, dateOfOrder));
            }
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:getAll " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(allOrdersStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }
}
