package presentation;

import bll.CustomerBLL;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import model.Customer;

public class ManageCustomersController {

    @FXML
    private TableView<Customer> customersTable;

    @FXML
    private TableColumn<Customer, String> addressCol;

    @FXML
    private TableColumn<Customer, String> nameCol;

    @FXML
    private TextField nameField;

    @FXML
    private TextField addressField;

    @FXML
    void addCustomer(ActionEvent event) {
        Customer customer = new Customer(1, nameField.getText(), addressField.getText());
        CustomerBLL customerBll = new CustomerBLL();
        int id = customerBll.insertCustomer(customer);
        if (id > 0) {
            MainController.displayInfo("", "Customer added to database", "Info");
            initTableCustomers();
        }
        emptyFields();
    }

    @FXML
    void deleteCustomer(ActionEvent event) {
        int currentCustomer = customersTable.getSelectionModel().getSelectedIndex();
        if (currentCustomer < 0) // nothing selected
        {
            return;
        }
        if (MainController.displayAlert("Do you want to delete this customer \n"
                + "and all the orders made by him?", "Delete customer?", "Are you sure?")) {
            Customer cust = customersTable.getItems().get(currentCustomer);
            CustomerBLL custBLL = new CustomerBLL();
            custBLL.deleteCustomer(cust);
            customersTable.getItems().remove(currentCustomer);
        }
    }

    @FXML
    void cancel(ActionEvent event) {
        Stage stage = (Stage) nameField.getScene().getWindow();
        stage.hide();
    }

    @FXML
    void initialize() {
        initTableCustomers();
        initTableCustomersProperties();
    }

    private void initTableCustomers() {
        customersTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        CustomerBLL customerBll = new CustomerBLL();
        customersTable.getItems().clear();
        customersTable.getItems().addAll(customerBll.getCustomers()); // read data from the database
    }

    private void emptyFields() {
        nameField.setText("");
        addressField.setText("");
    }

    private void initTableCustomersProperties() {
        nameCol.setCellValueFactory(
                new PropertyValueFactory<Customer, String>("name")
        // associates with the name field from Product
        );
        addressCol.setCellValueFactory(
                new PropertyValueFactory<Customer, String>("address")
        // associates with the quantity field from Product
        );

        // Make column name editable:
        nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
        nameCol.setOnEditCommit(
                new EventHandler<CellEditEvent<Customer, String>>() {
            @Override
            public void handle(CellEditEvent<Customer, String> t) {
                Customer cust = (Customer) t.getTableView().getItems().get(t.getTablePosition().getRow());
                cust.setName(t.getNewValue());

                if (update(cust)) {
                    initTableCustomers();
                }
            }
        });

        // Make column address editable:
        addressCol.setCellFactory(TextFieldTableCell.forTableColumn());
        addressCol.setOnEditCommit(
                new EventHandler<CellEditEvent<Customer, String>>() {
            @Override
            public void handle(CellEditEvent<Customer, String> t) {
                Customer cust = (Customer) t.getTableView().getItems().get(t.getTablePosition().getRow());
                cust.setAddress(t.getNewValue());

                if (update(cust)) {
                    initTableCustomers();
                }
            }
        });
    }

    private boolean update(Customer cust) { return (new CustomerBLL()).updateCustomer(cust);
    }
}
