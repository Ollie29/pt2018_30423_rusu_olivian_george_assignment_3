package presentation;

import bll.ProductBLL;
import java.util.ArrayList;
import java.util.Optional;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import model.Product;

public class ManageProductsController {

    @FXML
    private TableColumn<Product, Integer> qtyCol;

    @FXML
    private TableColumn<Product, String> nameCol;

    @FXML
    private TableColumn<Product, Float> priceCol;

    @FXML
    private TableView<Product> productsTable;

    @FXML
    private TextField priceField;

    @FXML
    private TextField nameField;

    @FXML
    private TextField qtyField;


    @FXML
    void addProduct(ActionEvent event) {
        int qty = 0;
        float price = 0;
        try {
            qty = Integer.parseInt(qtyField.getText());
            price = Float.parseFloat(priceField.getText());
        } catch (NumberFormatException numberFormatException) {
            return;
        }
        Product product = new Product(1, nameField.getText(), price, qty);
        ProductBLL productBll = new ProductBLL();
        int id = productBll.insertProduct(product);
        if (id > 0) {
            MainController.displayInfo("", "Product added to database", "Info");
            initTableProducts();
        }
        emptyFields();
    }

    @FXML
    void deleteProduct(ActionEvent event) {
        int currentProduct = productsTable.getSelectionModel().getSelectedIndex();
        if (currentProduct < 0) // nothing selected
        {
            return;
        }
        if (MainController.displayAlert("Do you want to delete this product \n"
                + "and all the orders that contain it?", "Delete product?", "Are you sure?")) {
            Product prod = productsTable.getItems().get(currentProduct);
            ProductBLL prodBLL = new ProductBLL();
            prodBLL.deleteProduct(prod);
            productsTable.getItems().remove(currentProduct);
        }
    }

    @FXML
    void cancel(ActionEvent event) {
        Stage stage = (Stage) nameField.getScene().getWindow();
        stage.hide();
    }

    @FXML
    void initialize() {
        initTableProducts();
        initTableProductsProperties();
    }

    private void initTableProducts() {
        productsTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        ProductBLL productBll = new ProductBLL();
        productsTable.getItems().clear();
        productsTable.getItems().addAll(productBll.getProducts()); // read data from the database    }
    }

    private void emptyFields() {
        qtyField.setText("");
        nameField.setText("");
        priceField.setText("");
    }

    private void initTableProductsProperties() {
        nameCol.setCellValueFactory(
                new PropertyValueFactory<Product, String>("name")
        // associates with the name field from Product
        );
        qtyCol.setCellValueFactory(
                new PropertyValueFactory<Product, Integer>("quantity")
        // associates with the quantity field from Product
        );
        priceCol.setCellValueFactory(
                new PropertyValueFactory<Product, Float>("price")
        // associates with the price field from Product
        );

        // Make column name editable:
        nameCol.setCellFactory(TextFieldTableCell.forTableColumn());
        nameCol.setOnEditCommit(
                new EventHandler<CellEditEvent<Product, String>>() {
            @Override
            public void handle(CellEditEvent<Product, String> t) {
                Product prod = (Product) t.getTableView().getItems().get(
                        t.getTablePosition().getRow());
                prod.setName(t.getNewValue());

                if (update(prod)) {
                    initTableProducts();
                }
            }
                });
            

        // Make column quantity editable (it has to be an integer, not a string, so it's slightly more complicated):
        qtyCol.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<Integer>(){
            @Override
            public Integer fromString(String s) {
                Integer qty = -1;
                try {
                    qty = Integer.parseInt(s);
                } catch (NumberFormatException numberFormatException) {
                }
                return qty;
            }

            @Override
            public String toString(Integer object) {
                return object.toString();
            }
    }));

        qtyCol.setOnEditCommit(
                new EventHandler<CellEditEvent<Product, Integer>>() {
            @Override
            public void handle(CellEditEvent<Product, Integer> t) {
                Product prod = (Product) t.getTableView().getItems().get(t.getTablePosition().getRow());
                prod.setQuantity(t.getNewValue());

                if (update(prod)) {
                    initTableProducts();
                }
            }
        }
        );
        
        // Make column price editable (it has to be an integer, not a string, so it's slightly more complicated than for string):
        priceCol.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<Float>(){
            @Override
            public Float fromString(String s) {
                Float price = -1.0f;
                try {
                    price = Float.parseFloat(s);
                } catch (NumberFormatException numberFormatException) {
                }
                return price;
            }

            @Override
            public String toString(Float object) {
                return object.toString();
            }
    }));

        priceCol.setOnEditCommit(
                new EventHandler<CellEditEvent<Product, Float>>() {
            @Override
            public void handle(CellEditEvent<Product, Float> t) {
                Product prod = (Product) t.getTableView().getItems().get(t.getTablePosition().getRow());
                prod.setPrice(t.getNewValue());

                if (update(prod)) {
                    initTableProducts();
                }
            }
        }
        );    }

    private boolean update(Product prod) {
        return (new ProductBLL()).updateProduct(prod);
    }
}
