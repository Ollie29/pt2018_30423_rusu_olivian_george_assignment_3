package presentation;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class Reflection {

    public static ArrayList<String> retrievePropertyNames(Object object) {
        ArrayList<String> fields = new ArrayList<>();
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true); // set modifier to public
            try {
                fields.add(field.getName());

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
        return fields;
    }
}
