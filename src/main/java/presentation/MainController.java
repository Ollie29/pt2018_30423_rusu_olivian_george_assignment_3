package presentation;

import bll.*;
import dao.OrderFull;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import dao.ProductDAO;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.*;

public class MainController {

    @FXML
    private DatePicker dateField;

    @FXML
    private ListView<String> customers;

    @FXML
    private TextField quantityField;

    @FXML
    private ListView<String> products;

    private ManageProductsController ctrlManageProducts;
    private Stage stageManageProduct;
    private Stage stageManageCustomers;
    private ManageCustomersController ctrlNewCustomer;
    ArrayList<Customer> allCustomers;
    ArrayList<Product> allProducts;

    // Orders tab:
    @FXML
    private TableView<OrderFull> ordersTable;

    @FXML
    private TableColumn<OrderFull, String> customerCol;

    @FXML
    private TableColumn<OrderFull, Date> dateCol;

    @FXML
    private TableColumn<OrderFull, Integer> quantityCol;

    @FXML
    private TableColumn<OrderFull, String> productCol;

    @FXML
    private TableColumn<OrderFull, Float> priceCol;

    // Stock tab:
    @FXML
    TableView<Product> stocksTable;

    @FXML
    private TableColumn<Product, Float> unitPriceCol;

    @FXML
    private TableColumn<Product, String> nameCol;

    @FXML
    private TableColumn<Product, Integer> qtyCol;

    @FXML
    void closeApp(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void manageProducts(ActionEvent event) {
        if (stageManageProduct == null) {
            initWndManageProducts();
        }
        ctrlManageProducts.initialize();
        stageManageProduct.showAndWait();
        initListProducts();
        initTableOrders();
        initTableStocks();
    }

    @FXML
    void manageCustomers(ActionEvent event) {
        if (stageManageCustomers == null) {
            initWndManageCustomers();
        }
        stageManageCustomers.showAndWait();
        initListCustomers();
        initTableOrders();
    }

    @FXML
    void about(ActionEvent event) {
        displayInfo("Programming Techniques - Homework 3 (2017-2018)\n"
                + "Olivian Rusu (30423)", "Application for Order Management in a Warehouse",
                "About");
    }

    @FXML
    void addOrder(ActionEvent event) {
        int custIndex = customers.getSelectionModel().getSelectedIndex();
        if (custIndex < 0) // nothing selected
        {
            return;
        }
        int prodIndex = products.getSelectionModel().getSelectedIndex();
        if (prodIndex < 0) // nothing selected
        {
            return;
        }
        LocalDate ordDate = dateField.getValue();
        if (ordDate == null) {
            return;
        }
        Date orderDate = convert(ordDate);

        int qty = 0;
        try {
            qty = Integer.parseInt(quantityField.getText());
        } catch (NumberFormatException numberFormatException) {
            return;     // quantity introduced is not an integer
        }
        Order o = new Order(1, // the id will be changed after inserting in table
                allProducts.get(prodIndex).getId(),
                allCustomers.get(custIndex).getId(),
                qty, orderDate);
        Product p = ProductDAO.findById(o.getProductId());
        if(o.getQuantity() > p.getQuantity()){
            displayInfo("", "Stock insufficient", "Info");
        }
        OrderBLL orderBll = new OrderBLL();
        int id = orderBll.insertOrder(o);
        if (id > 0) {
            displayInfo("", "Order added to database", "Info");
            initTableOrders(); //takes data from database
            initTableStocks(); //takes data from database
        }
        else
            displayInfo("", "UnderStock", "Info");
        emptyFields(); //empties the fields
    }

    @FXML
    void deleteOrder(ActionEvent event) {
        int currentOrder = ordersTable.getSelectionModel().getSelectedIndex();
        if (currentOrder < 0) // nothing selected
        {
            return;
        }
        OrderFull order = ordersTable.getItems().get(currentOrder);
        OrderBLL orderBLL = new OrderBLL();
        orderBLL.deleteOrder(new Order(order.getOrderId(), order.getProductId(),
                order.getCustomerId(), order.getQuantity(), order.getDateOfOrder()));
        ordersTable.getItems().remove(currentOrder);
        initTableStocks();
        ctrlManageProducts.initialize(); // because deleting an order changes quantity for some product
        initTableStocks();
    }

    @FXML
    void makeReceipt(ActionEvent event) {
        int currentOrder = ordersTable.getSelectionModel().getSelectedIndex(); // used for selection in tableView
        if (currentOrder < 0) // nothing selected
        {
            return;
        }
        OrderFull order = ordersTable.getItems().get(currentOrder);
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("order " +order.getOrderId() +
                        ".txt"), "utf-8"))) {
            writer.write("Order number: " + order.getOrderId());
            writer.newLine();
            writer.write("Date of order: " + order.getDateOfOrder());
            writer.newLine();
            writer.write("Customer: " + order.getCustomer());
            writer.newLine();
            writer.write("Product: " + order.getProduct());
            writer.newLine();
            writer.write("Quantity: " + order.getQuantity());
            writer.newLine();
            writer.write("Total price: " + order.getTotalPrice());
            displayInfo("In file order " + order.getOrderId() +
                        ".txt", "Receipt generated", "Information");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.WARNING, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    @FXML
    void initialize() {

        initListCustomers();
        initListProducts();
        initTableOrders();
        initPropertiesTableOrders();
        initTableStocks();
        initPropertiesTableStocks();
    }

    private void initWndManageProducts() {
        try {
            // load fxml file for the window
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ManageProducts.fxml"));
            // load the container having the window contents
            AnchorPane container = (AnchorPane) loader.load();
            // get the controller object of the window
            ctrlManageProducts = loader.getController();
            // create the object for the window:
            stageManageProduct = new Stage();
            // set the title of the created window
            stageManageProduct.setTitle("New product in the warehouse");
            // the window will show as MODAL
            stageManageProduct.initModality(Modality.APPLICATION_MODAL);
            // create the scene containing the window interface
            Scene scene = new Scene(container);
            // atach the scene (content of the window) to the stage (window)
            stageManageProduct.setScene(scene);
        } catch (IOException e) {
            // Exception gets thrown if the fxml file could not be loaded
            /////////////////////////////// TODO logger
        }
    }

    private void initWndManageCustomers() {
        try {
            // load fxml file for the window
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ManageCustomers.fxml"));
            // load the container having the window contents
            AnchorPane container = (AnchorPane) loader.load();
            // get the controller object of the window
            ctrlNewCustomer = loader.getController();
            // create the object for the window:
            stageManageCustomers = new Stage();
            // set the title of the created window
            stageManageCustomers.setTitle("New client for the warehouse");
            // the window will show as MODAL
            stageManageCustomers.initModality(Modality.APPLICATION_MODAL);
            // create the scene containing the window interface
            Scene scene = new Scene(container);
            // atach the scene (content of the window) to the stage (window)
            stageManageCustomers.setScene(scene);
        } catch (IOException e) {
            // Exception gets thrown if the fxml file could not be loaded
            /////////////////////////////// TODO logger
        }
    }

    private void initListCustomers() {
        CustomerBLL customerBll = new CustomerBLL();
        allCustomers = customerBll.getCustomers();
        customers.getItems().clear();
        for (Customer c : allCustomers) {
            customers.getItems().add(c.getName());
        }
    }

    private void initListProducts() {
        ProductBLL productBll = new ProductBLL();
        allProducts = productBll.getProducts();
        products.getItems().clear();
        for (Product p : allProducts) {
            products.getItems().add(p.getName());
        }
    }

    private Date convert(LocalDate ordDate) {
        return Date.valueOf(ordDate.toString());
    }

    public static void displayInfo(String infoText, String header, String title) {
        Alert alert = new Alert(AlertType.INFORMATION, infoText);
        alert.setHeaderText(header);
        alert.setTitle(title);
        alert.showAndWait();
    }

    public static boolean displayAlert(String infoText, String header, String title) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, infoText);
        alert.setHeaderText(header);
        alert.setTitle(title);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            return true;
        } else {
            return false;
        }
    }

    private void emptyFields() {
        dateField.setValue(LocalDate.now());
        quantityField.setText("");
        customers.getSelectionModel().clearSelection();
        products.getSelectionModel().clearSelection();
    }

    private void initTableOrders() {
        ordersTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        OrderBLL orderBll = new OrderBLL();
        ordersTable.getItems().clear();
        ordersTable.getItems().addAll(orderBll.getOrders()); // read data from the database

    }

   void initTableStocks() {
        stocksTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        ProductBLL productBll = new ProductBLL();
        stocksTable.getItems().clear();
        stocksTable.getItems().addAll(productBll.getStocks()); // read data from the database
    }

    private void initPropertiesTableOrders() {
        customerCol.setCellValueFactory(
                new PropertyValueFactory<OrderFull, String>("customer")
        );
        productCol.setCellValueFactory(
                new PropertyValueFactory<OrderFull, String>("product")
        );
        quantityCol.setCellValueFactory(
                new PropertyValueFactory<OrderFull, Integer>("quantity")
        );
        priceCol.setCellValueFactory(
                new PropertyValueFactory<OrderFull, Float>("totalPrice")
        );
        dateCol.setCellValueFactory(
                new PropertyValueFactory<OrderFull, Date>("dateOfOrder")
        );
    }

    ////TableView header for stocks
    private void initPropertiesTableStocks() {
        nameCol.setCellValueFactory(
                new PropertyValueFactory<Product, String>("name")
        );
        unitPriceCol.setCellValueFactory(
                new PropertyValueFactory<Product, Float>("price")
        );
        qtyCol.setCellValueFactory(
                new PropertyValueFactory<Product, Integer>("quantity")
        );

        // use reflection to display column names in table of stocks
        Product p = new Product(1, "a", 1.0f, 1);   // create a random Product object to read its field names
        ArrayList<String> names = Reflection.retrievePropertyNames(p);
        nameCol.setText(names.get(1));
        unitPriceCol.setText(names.get(2));
        qtyCol.setText(names.get(3));

    }
}
