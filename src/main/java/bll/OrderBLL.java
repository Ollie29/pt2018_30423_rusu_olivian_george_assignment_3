package bll;

import bll.validators.OrderQuantityValidator;
import bll.validators.Validator;
import dao.OrderDAO;
import dao.OrderFull;
import dao.ProductDAO;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import model.Order;
import model.Product;

public class OrderBLL {

    private List<Validator<Order>> validators;

    public OrderBLL() {
		validators = new ArrayList<Validator<Order>>();
		validators.add(new OrderQuantityValidator());
        
    }

    public Order findOrderById(int id) {
        Order ord = OrderDAO.findById(id);
        if (ord == null) {
            throw new NoSuchElementException("The order with id =" + id + " was not found!");
        }
        return ord;
    }

    public int insertOrder(Order order) {
        for (Validator<Order> v : validators) {
            v.validate(order);
        }
        int insertedId = OrderDAO.insert(order);
        if (insertedId > 0) {
            // order comitted, therefore decrease stock for the product:
            Product p = ProductDAO.findById(order.getProductId());
            int qty = p.getQuantity();
            qty -= order.getQuantity();
            p.setQuantity(qty);
            ProductDAO.update(p);
        }
        return  insertedId;
    }
    
    public void deleteOrder(Order order) {
        Product p = ProductDAO.findById(order.getProductId());
        int qty = order.getQuantity();
        OrderDAO.deleteOrder(order);
        // after removing an order, the product stock increases with that quantity
        p.setQuantity(p.getQuantity()+qty);
        ProductDAO.update(p);
    }
    
    public ArrayList<OrderFull> getOrders() {
        return OrderDAO.getAll();
    }
    
}
