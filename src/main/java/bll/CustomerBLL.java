package bll;

import bll.validators.CustomerValidator;
import bll.validators.Validator;
import dao.CustomerDAO;
import dao.OrderDAO;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import model.Customer;

public class CustomerBLL {

    private List<Validator<Customer>> validators;

    public CustomerBLL() {
        validators = new ArrayList<Validator<Customer>>();
        validators.add(new CustomerValidator());
    }

	public Customer findCustomerById(int id) {
		Customer c = CustomerDAO.findById(id);
		if (c == null) {
			throw new NoSuchElementException("The customer with id =" + id + " was not found!");
		}
		return c;
	}

	public int insertCustomer(Customer customer) {

        for(Validator<Customer> v: validators ){
            v.validate(customer);
        }
        return CustomerDAO.insert(customer);
	}
        
	public ArrayList<Customer> getCustomers() {
            return CustomerDAO.getAll();
        }
    
    public void deleteCustomer(Customer customer) {
        CustomerDAO.deleteCustomer(customer);
        OrderDAO.deleteCustomerOrders(customer.getId());
    }

    public boolean updateCustomer(Customer customer) {
        for (Validator<Customer> v : validators) {
            v.validate(customer);
        }
        return CustomerDAO.update(customer);
    }
}
