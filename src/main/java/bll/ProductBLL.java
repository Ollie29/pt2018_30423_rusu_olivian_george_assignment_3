package bll;

import bll.validators.*;
import dao.OrderDAO;
import dao.ProductDAO;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import model.Product;

public class ProductBLL {

    private List<Validator<Product>> validators;

    public ProductBLL() {
        validators = new ArrayList<Validator<Product>>();
        validators.add(new ProductPriceValidator());
        validators.add(new ProductQuantityValidator());
        validators.add(new ProductNameValidator());
    }

    public Product findProductById(int id) {
        Product p = ProductDAO.findById(id);
        if (p == null) {
            throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
        return p;
    }

    public int insertProduct(Product product) {
        for (Validator<Product> v : validators) {
            v.validate(product);
        }
        return ProductDAO.insert(product);
    }

    public ArrayList<Product> getProducts() {
        return ProductDAO.getAll();
    }
    
    public ArrayList<Product> getStocks() {
        return ProductDAO.getInStock();
    }    

    public void deleteProduct(Product product) {
        ProductDAO.deleteProduct(product);
        OrderDAO.deleteProductOrders(product.getId());
    }
    
    public boolean updateProduct(Product product) {
        for (Validator<Product> v : validators) {
            v.validate(product);
        }
        return ProductDAO.update(product);
    }
}
