package bll.validators;

import model.Product;

public class ProductPriceValidator implements Validator<Product> {
    @Override
    public void validate(Product p) {
        if (p.getPrice() <= 0) {
            throw new IllegalArgumentException("Price is negative!");
        }
    }
    
}
