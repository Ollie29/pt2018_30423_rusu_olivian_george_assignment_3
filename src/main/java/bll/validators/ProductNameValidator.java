package bll.validators;

import model.Product;

public class ProductNameValidator implements Validator<Product> {
    @Override
    public void validate(Product p) {
        if (p.getName() == null || p.getName().trim().equals("")) {
            throw new IllegalArgumentException("Product name is empty!");
        }
    }
    
}
