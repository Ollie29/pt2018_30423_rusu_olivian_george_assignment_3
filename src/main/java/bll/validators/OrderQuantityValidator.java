package bll.validators;

import dao.ProductDAO;
import model.Order;
import model.Product;

public class OrderQuantityValidator implements Validator<Order> {
    @Override
    public void validate(Order ord) {
        if (ord.getQuantity() <= 0) {
            throw new IllegalArgumentException("Quantity is not strictly positive!");
        }
        // Check if the required quantity is in stock:
        Product p = ProductDAO.findById(ord.getProductId());
        if (ord.getQuantity() > p.getQuantity()) {
            throw new IllegalArgumentException("Quantity is greater than available!");
        }
    }
    
}
