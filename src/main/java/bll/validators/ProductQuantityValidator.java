package bll.validators;

import model.Product;

public class ProductQuantityValidator implements Validator<Product> {

    @Override
    public void validate(Product p) {
        if (p.getQuantity() < 0) {
            throw new IllegalArgumentException("Quantity is negative!");
        }
    }

}
