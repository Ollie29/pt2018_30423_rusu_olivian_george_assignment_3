package bll.validators;

import model.Customer;

public class CustomerValidator implements Validator<Customer> {
    @Override
    public void validate(Customer c) {
        if (c.getName() == null || c.getName().trim().equals("")) {
            throw new IllegalArgumentException("Customer name is empty!");
        }
        if (c.getAddress() == null || c.getAddress().trim().equals("")) {
            throw new IllegalArgumentException("Customer address is empty!");
        }
    }
    
    
}
